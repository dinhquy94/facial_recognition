import matplotlib.pyplot as plt
import numpy as np
from mongo import redis_db
from helpers import img_encoder
import model
import config
import json
from PIL import Image
import io
import mongo
import pickle
from mongoengine import *
import time
import cv2

from schemas.faces_store_schema import FacesStores

verification_threshhold = 0.58
image_size = 160

v = model.v
d = model.d


def img_to_encoding(image):
    faces = d.align(image, True)

    encoded = []
    for face in faces:
        encoded.append(v.img_to_encoding(face, image_size))
    return encoded


def one_img_to_encoding(image):
    faces = d.align(image, True)
    aligned = faces[0]
    return v.img_to_encoding(aligned, image_size)


def distance(emb1, emb2):
    diff = np.subtract(emb1, emb2)
    return np.sum(np.square(diff))


def recognize(image, database):
    faces_encoding = img_to_encoding(image)
    abort = []
    result = []
    for encoding in faces_encoding:
        for face_in_database in database:
            if face_in_database not in abort:
                # Step 2: Compute distance with identity's image
                dist = distance(encoding, database[face_in_database])
                # Step 3: Open the door if dist < verification_threshhold, else don't open
                if dist < verification_threshhold:
                    abort.append(face_in_database)
                    try:
                        face = FacesStores.objects.get(face_id=face_in_database)
                        result.append({
                            'name': face.name,
                            'username_mapping': face.username_mapping
                            # 'face_id': face_in_database,
                            # 'distance': dist
                        })

                    except DoesNotExist:
                        print("Err when get face info")

    return result


def recognition_process():
    # load the pre-trained Keras model (here we are using a model
    # pre-trained on ImageNet and provided by Keras, but you can
    # substitute in your own networks just as easily)
    print("* Loading model...")

    print("* Model loaded")

    # continually pool for new images to classify
    while True:
        # attempt to grab a batch of images from the database, then
        # initialize the image IDs and batch of images themselves
        queue = redis_db.lrange(config.FACIAL_DETECTOR_QUEUE, 0,
                                config.BATCH_SIZE - 1)
        # imageIDs = []
        # batch = None

        # loop over the queue
        for q in queue:
            # deserialize the object and obtain the input image
            q = json.loads(q)
            if q['type'] != 'update_face_encoding':
                image = img_encoder.base64_decode_image(q["image"])
                image = Image.open(io.BytesIO(image))
                image = np.array(image)

            if q['type'] == 'detect':
                database = {}
                # load database
                redis_cache = redis_db.get(q["store_id"])
                if redis_cache is None:
                    try:
                        faces_in_stores = FacesStores.objects(store_id=q["store_id"])
                        for face in faces_in_stores:
                            face.last_coming_time = time.time()
                            database[face.face_id] = img_file_to_encoding("./images/faces/" + face.face_id + '.jpg')
                        redis_db.set(q["store_id"], pickle.dumps(database))
                    except:
                        # do nothing
                        database = {}
                else:
                    database = pickle.loads(redis_cache)
                redis_db.set(q["id"], json.dumps(recognize(image, database)))
            elif q['type'] == 'compare':
                image2 = img_encoder.base64_decode_image(q["image2"])
                image2 = Image.open(io.BytesIO(image2))
                image2 = np.array(image2)
                distance_between = distance(one_img_to_encoding(image), one_img_to_encoding(image2))
                redis_db.set(q["id"], json.dumps({"distance": distance_between}))
            elif q['type'] == 'update_face_encoding':
                try:
                    face_store = FacesStores.objects.get(
                        store_id=q['store_id'],
                        face_id=q['id']
                    )
                    face_store.face_encoding = img_file_to_encoding_without_aligment("./images/faces/" + q['id'] + '.jpg')
                    face_store.save()
                except DoesNotExist:
                    print('face_id not found')

            redis_db.ltrim(config.FACIAL_DETECTOR_QUEUE, 1, -1)


def img_file_to_encoding(img):
    image = plt.imread(img)
    faces = d.align(image, True)
    aligned = faces[0]
    plt.imshow(aligned)
    encoded = []
    for face in faces:
        encoded.append(v.img_to_encoding(face, image_size))
    return encoded

def img_file_to_encoding_without_aligment(image):
    # img = plt.imread(image)
    img = cv2.imread(image)
    encoded = []
    img = cv2.resize(img, (image_size, image_size))
    encoded.append(v.img_to_encoding(img, image_size))
    return encoded

if __name__ == "__main__":
    recognition_process()



