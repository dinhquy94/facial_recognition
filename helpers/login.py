import mongo
import jwt
import config
import datetime
from helpers import parser
import time
from schemas.user_schema import Users
from mongoengine import *


def login(username, password_hashed):
    try:
        user = Users.objects.get(username=username, password_hashed=password_hashed)
        user_info = {
            'username': user.username,
            'exp': time.time() + 1 * 60 * 60 * 60,
            '_id': str(user.id)
        }
        return True, jwt.encode(parser.mongoObjToJson(user_info), config.APP_SECRET).decode('utf-8')
    except DoesNotExist:
        return False, ''


def get_info(username):
    user = mongo.db.users.find_one({'username': username})
    return parser.mongoObjToJson(user)


def is_login(access_token):
    try:
        decoded = jwt.decode(access_token, config.APP_SECRET, 'utf-8')
        return decoded
    except:
        return False

def getUser(request):
    user = jwt.decode(request.args.get('access_token'), config.APP_SECRET, 'utf-8')
    user['_id'] = user['_id']
    return user
