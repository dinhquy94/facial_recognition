import json
from bson.json_util import dumps


def mongoObjToJson(bson_data):
    return json.loads(dumps(bson_data))
