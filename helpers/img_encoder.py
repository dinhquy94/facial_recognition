# import the necessary packages
import numpy as np
import base64
import sys
import uuid
import cv2


def base64_encode_image(file):
    npimg = np.fromfile(file, np.uint8)
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    retval, buffer = cv2.imencode('.jpg', img)
    img1_bytes = np.array(buffer).tostring()
    jpg_as_text = base64.b64encode(img1_bytes)
    return jpg_as_text


def base64_decode_image(img_text):
    return base64.b64decode(img_text)
