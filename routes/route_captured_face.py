from flask import request
from flask_restful import Resource
import json
from mongoengine import *
from schemas.captured_face import CapturedFace

class RouteCapturedFace(Resource):

    def get(self):
        stores = CapturedFace.objects(store_id=request.args.get("store_id"))
        return json.loads(stores.to_json()), 200

    def delete(self):
        try:
            store = CapturedFace.objects.get(store_id=request.args.get("store_id"), face_id=request.args.get('face_id'))
            store.delete()

            return {'message': 'OK'}, 200
        except DoesNotExist:
            return {'message': 'face_id not found'}, 404