from flask import request
from flask_restful import Resource
import hashlib
from helpers import parser
from schemas.user_schema import Users
from schemas.store_schema import Stores
import json

class RouteRegister(Resource):

    def post(self):
        data = request.get_json()
        try:
            user = Users(
                username=data['username'],
                password_hashed=hashlib.md5(data['password'].encode('utf-8')).hexdigest(),
                email=data['email'],
            )
            if 'description' in data:
                user.description = data['description']
            user.save()
            data['id'] = str(user.id)
            return parser.mongoObjToJson(user.to_mongo()), 200
        except Exception as e:
            return {'message': str(e)}, 400
    def get(self):
        try:
            store = Stores.objects.get(
                store_id=request.args.get("store_id"),
                api_key=request.args.get("api_key")
            )
            return json.loads(store.to_json()), 200
        except Exception as e:
            return {'message': "invalid key"}, 401