from helpers.login import login
from helpers.login import is_login

from flask import request
from flask_restful import Resource
import hashlib


class Login(Resource):

    def post(self):
        data = request.get_json()
        login_result, token = login(data['username'], hashlib.md5(data['password'].encode('utf-8')).hexdigest())
        if login_result:
            return {'token': str(token)}, 200
        else:
            return {'err': 'invalid'}, 401
    def get(self):
        jwt = request.args.get('access_token')
        user = is_login(str(jwt))
        if user:
            return user, 200
        else:
            return {'result': False}, 401