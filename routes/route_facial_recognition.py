from flask_restful import Resource
from flask import request
import uuid
from helpers import img_encoder
from mongo import redis_db
import config
import json
import mongo
from helpers.login import getUser
from flask import send_file
from schemas.store_schema import Stores
from schemas.faces_store_schema import FacesStores

class   RouteFacialRecognition(Resource):
    def post(self):
        data = request.form

        if not data:
            return {"err": 'no data 3'}, 500
        # check store_id
        if 'store_id' in data:
            store = mongo.db.stores.find_one({
                'store_id': data['store_id'],
                'api_key': request.args.get("api_key")
            })
            if not store:
                return {'err': 'store_id not found'}, 404
        if ('quota' in store) and request.files.get("image") and store['quota'] > 0:

            jpg_as_text = img_encoder.base64_encode_image(request.files['image'])
            k = str(uuid.uuid4())

            d = {}
            if request.files.get("image2") and (request.args.get("action") == 'compare'):
                d = {
                    "id": k,
                    "image": jpg_as_text.decode("utf-8"),
                    "store_id": data['store_id'],
                    "type": 'compare',
                    "image2": img_encoder.base64_encode_image(request.files['image2']).decode("utf-8")
                }
            else:
                d = {
                    "id": k,
                    "image": jpg_as_text.decode("utf-8"),
                    "store_id": data['store_id'],
                    "type": 'detect'
                }
            import time
            start = time.time()

            redis_db.rpush(config.FACIAL_DETECTOR_QUEUE, json.dumps(d))
            # keep looping until our model server returns the output
            # predictions
            while True:
                # attempt to grab the output predictions
                output = redis_db.get(k)

                # check to see if our model has classified the input
                # image
                if output is not None:
                    # add the output predictions to our data
                    # dictionary so we can return it to the client
                    output = output.decode("utf-8")
                    "the code you want to test stays here"
                    end = time.time()
                    print(end - start)
                    # delete the result from the database and break
                    # from the polling loop
                    redis_db.delete(k)
                    store_info = Stores.objects.get(store_id=data['store_id'])
                    store_info.quota = store_info.quota - 1
                    store_info.save()
                    print('output length')
                    print(len(output))

                    if len(output) == 2:
                        print('output image')
                        f = request.files['image']
                        #save file
                        k = str(uuid.uuid4())
                        f.save(config.FACES_FOLDER,  k + ".jpg")
                        #create new customer
                        face_store = FacesStores(
                            store_id=data['store_id'],
                            face_id=k,
                            name='New customer',
                            description='New customer',
                            file_type='jpg'
                        )
                        face_store.save()
                        #set new customer data
                    return json.loads(output), 200
                    break

                # sleep for a small amount to give the model a chance
                # to classify the input image
                time.sleep(config.CLIENT_SLEEP)
        else:
            return {'err': 'img not found or max quota limited'}, 400

    def get(self):
        if request.args.get("face_id") is None:
            return {'message': 'img not found'}, 404

        return send_file(config.FACES_FOLDER + request.args.get("face_id") + '.jpg', mimetype='image/jpg')