
from helpers import parser
from helpers.login import getUser
import uuid

from flask import request
from flask_restful import Resource
import uuid
from schemas.store_schema import Stores
import json
from mongoengine import *
from schemas.faces_store_schema import FacesStores

class RouteStore(Resource):

    def post(self):

        user = getUser(request)
        data = request.get_json()

        try:
            store = Stores(
                user_id=user['_id'],
                store_id= str(uuid.uuid4()),
                store_name=data['store_name'],
                api_key=str(uuid.uuid4())
            )
            if 'description' in data:
                store.description = data['description']

            store.save()
            data['id'] = str(store.id)
            return parser.mongoObjToJson(store.to_mongo()), 200
        except Exception as e:
            return {'message': str(e)}, 400

    def get(self):
        user = getUser(request)
        stores = Stores.objects(user_id=user['_id'])
        for store in stores:
            store.count = FacesStores.objects(store_id=store.store_id).count()
            store.save()
        return json.loads(stores.to_json()), 200

    def delete(self):
        try:
            store = Stores.objects.get(store_id=request.args.get("store_id"))
            store.delete()

            return {'message': 'OK'}, 200
        except DoesNotExist:
            return {'message': 'Database not found'}, 404