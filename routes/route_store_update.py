from helpers import parser
from helpers.login import getUser
import os
from flask import request
from flask_restful import Resource
from schemas.faces_store_schema import FacesStores
from schemas.store_schema import Stores
from mongoengine import *

import config
from mongo import redis_db
import json

class RouteStoreUpdate(Resource):

    def post(self):
        data = request.get_json()
        # user = getUser(request)

        try:
            Stores.objects.get(store_id=data['store_id'])
            if 'face_id' in data and os.path.exists(config.FACES_FOLDER + data['face_id'] + '.jpg'):
                try:
                    FacesStores.objects.get(store_id=data['store_id'], face_id=data['face_id'])
                    return {'err': 'face_id is existed in the store'}, 500
                except DoesNotExist:
                    face_store = FacesStores(
                        store_id=data['store_id'],
                        face_id=data['face_id'],
                        username_mapping=data['username_mapping'],
                        name=data['name'],
                        description=data['description'],
                        file_type='jpg'
                    )
                    if 'username_mapping' in data:
                        face_store.username_mapping = data['username_mapping']
                    face_store.save()
                    d = {
                        "id": data['face_id'],
                        "image": "",
                        "store_id": data['store_id'],
                        "type": 'update_face_encoding'
                    }
                    redis_db.rpush(config.FACIAL_DETECTOR_QUEUE, json.dumps(d))
                    redis_db.delete(data['store_id'])
                return parser.mongoObjToJson(face_store.to_mongo()), 200
            else:
                return {'err': 'face_id not found'}
        except DoesNotExist:
            return {'err': 'store_id not found'}

    def put(self):
        data = request.get_json()
        # user = getUser(request)
        try:
            Stores.objects.get(store_id=data['store_id'])
            if 'face_id' in data and os.path.exists(config.FACES_FOLDER + data['face_id'] + '.jpg'):
                try:
                    FacesStores.objects.get(store_id=data['store_id'], face_id=data['face_id'])
                    face_store = FacesStores.objects.get(
                        store_id=data['store_id'],
                        face_id=data['face_id']
                    )
                    face_store.description = str(data['description'])
                    if 'username_mapping' in data:
                        face_store.username_mapping = data['username_mapping']
                    face_store.name = data['name']
                    face_store.save()
                    redis_db.delete(data['store_id'])
                except DoesNotExist:
                    return {'err': 'face_id not found'}, 404
                return parser.mongoObjToJson(face_store.to_mongo()), 200
            else:
                return {'err': 'face_id not found'}
        except DoesNotExist:
            return {'err': 'store_id not found'}

    def delete(self):

        try:
            face_store = FacesStores.objects.get(
                store_id=request.args.get("store_id"),
                face_id=request.args.get("face_id")
            )
            face_store.delete()
            redis_db.delete(request.args.get("store_id"))
            return {'err': 'Success'}
        except DoesNotExist:
            return {'err': 'store not found'}, 404

    def get(self):
        # user = getUser(request)
        try:
            Stores.objects.get(store_id=request.args.get("store_id"))
            faces_in_stores = FacesStores.objects(store_id=request.args.get("store_id"))

            return json.loads(faces_in_stores.to_json()), 200
        except DoesNotExist:
            return {'err': 'store_id not found'}, 404
