
from helpers import parser
from helpers.login import getUser
import uuid

from flask import request
from flask_restful import Resource
import uuid
from schemas.store_schema import Stores
import json
from mongoengine import *
from schemas.faces_store_schema import FacesStores


class RouteUsername(Resource):
    #get incoming person
    def get(self):
        try:
            Stores.objects.get(store_id=request.args.get("store_id"))
            faces_in_stores = FacesStores.objects(store_id=request.args.get("store_id"), username_mapping=request.args.get("username_mapping"))
            return json.loads(faces_in_stores.to_json()), 200
        except DoesNotExist:
            return {'err': 'store_id not found'}, 404

    def delete(self):
        data = request.get_json()
        try:
            face_store = FacesStores.objects.get(
                store_id=data['store_id'],
                face_id=data['face_id']
            )
            face_store.delete()
            return {'err': 'OK'}
        except DoesNotExist:
            return {'err': 'face not found'}, 404