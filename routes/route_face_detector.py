
from flask_restful import Resource
from flask import request
import uuid
from helpers import img_encoder
from mongo import redis_db
import config
import json
import time
import mongo
from helpers import parser

class RouteFaceDetector(Resource):
    def post(self):
        if request.files.get("image"):
            jpg_as_text = img_encoder.base64_encode_image(request.files['image'])
            k = str(uuid.uuid4())

            d = {
                "id": k,
                "image": jpg_as_text.decode("utf-8"),
                "store_id": request.args.get("store_id")
            }

            redis_db.rpush(config.IMAGE_QUEUE, json.dumps(d))
            # keep looping until our model server returns the output
            # predictions
            while True:
                # attempt to grab the output predictions
                output = redis_db.get(k)

                # check to see if our model has classified the input
                # image
                if output is not None:
                    # add the output predictions to our data
                    # dictionary so we can return it to the client
                    output = output.decode("utf-8")
                    # delete the result from the database and break
                    # from the polling loop
                    redis_db.delete(k)
                    return json.loads(output), 200

                    break

                # sleep for a small amount to give the model a chance
                # to classify the input image
                time.sleep(config.CLIENT_SLEEP)

    def get(self):

        face = mongo.db.faces_store.find({
            'face_id': {'$in': request.args.get("face_ids").split(',')}
        })
        return parser.mongoObjToJson(face), 200

