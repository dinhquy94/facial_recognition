# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/nguyendinhquy/Desktop/giaodien.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

VERSION = "Cam_display v0.10"
import PIL.Image, PIL.ImageTk
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import imread
import FaceToolKit as ftk
import DetectionToolKit as dtk
import time
import sys, time, threading, cv2
from PyQt5.QtCore import QTimer, QPoint, pyqtSignal
from PyQt5.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QPushButton
from PyQt5.QtWidgets import QWidget, QAction, QVBoxLayout, QHBoxLayout
from PyQt5.QtGui import QFont, QPainter, QImage, QTextCursor

try:
    import Queue as Queue
except:
    import queue as Queue

IMG_SIZE = 1280, 720  # 640,480 or 1280,720 or 1920,1080
IMG_FORMAT = QImage.Format_RGB888
DISP_SCALE = 3  # Scaling factor for display image
DISP_MSEC = 50  # Delay between display cycles
CAP_API = cv2.CAP_ANY  # API: CAP_ANY or CAP_DSHOW etc...
EXPOSURE = 0  # Zero for automatic exposure
TEXT_FONT = QFont("Courier", 10)

camera_num = 1  # Default camera (first in list)
image_queue = Queue.Queue()  # Queue to hold images
capturing = True  # Flag to indicate capturing


# Grab images from the camera (separate thread)
def grab_images(cam_num, queue):
    cap = cv2.VideoCapture(cam_num - 1 + CAP_API)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, IMG_SIZE[0])
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, IMG_SIZE[1])
    if EXPOSURE:
        cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)
        cap.set(cv2.CAP_PROP_EXPOSURE, EXPOSURE)
    else:
        cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
    while capturing:
        if cap.grab():
            retval, image = cap.retrieve(0)
            if image is not None and queue.qsize() < 2:
                queue.put(image)
            else:
                time.sleep(DISP_MSEC / 1000.0)
        else:
            print("Error: can't grab camera image")
            break
    cap.release()


class ImageWidget(QWidget):
    def __init__(self, parent=None):
        super(ImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        self.setMinimumSize(image.size())
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QPoint(0, 0), self.image)
        qp.end()

class Ui_Form(QMainWindow):
    text_update = pyqtSignal(str)

    def setupUi(self, Form):
        Form.setObjectName("Form")

        Form.resize(793, 455)
        Form.setMaximumSize(QtCore.QSize(800, 480))
        self.graphicsView = ImageWidget(Form)
        self.graphicsView.setGeometry(QtCore.QRect(10, 50, 561, 331))
        self.graphicsView.setObjectName("graphicsView")

        self.graphicsView.setAutoFillBackground(True)
        self.dateEdit = QtWidgets.QDateEdit(Form)
        self.dateEdit.setGeometry(QtCore.QRect(640, 20, 141, 24))
        self.dateEdit.setObjectName("dateEdit")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(540, 20, 111, 20))
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(408, 390, 161, 51))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("camera-1724286_960_720.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton.setIcon(icon)
        self.pushButton.setObjectName("pushButton")
        self.textBrowser = QtWidgets.QTextEdit(Form)

        self.textBrowser.setGeometry(QtCore.QRect(580, 280, 211, 111))
        self.textBrowser.setObjectName("textBrowser")
        self.textBrowser.setFont(TEXT_FONT)
        self.text_update.connect(self.append_text)
        sys.stdout = self

        self.pushButton_2 = QtWidgets.QPushButton(Form)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 390, 181, 51))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("Download-Icon-2.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_2.setIcon(icon1)
        self.pushButton_2.setObjectName("pushButton_2")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(670, 420, 118, 20))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")

        # Set up register table
        self._table = QtWidgets.QTableWidget(Form)
        self._table.setGeometry(QtCore.QRect(580, 50, 211, 221))

        self._table.setColumnCount(2)
        self._table.setHorizontalHeaderLabels(['User', 'TIME'])
        self._table.horizontalHeader().setStretchLastSection(True)
        self._table.verticalHeader().setVisible(False)

        #
        #        self.tableWidget.setGeometry(QtCore.QRect(580, 50, 211, 221))
        #        self.tableWidget.setObjectName("listView")
        #
        #
        #        self.tableWidget.setHorizontalHeaderLabels(['Col 1', 'Col 2', 'Col 3'])
        #        self.tableWidget.setRowCount(20)
        #        self.tableWidget.setColumnCount(3)
        #

        self.label_2 = QtWidgets.QLabel(Form)

        self.label_2.setGeometry(QtCore.QRect(10, 9, 521, 31))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Máy chấm công"))
        self.label.setText(_translate("Form", "Ngày hiện tại"))
        self.pushButton.setText(_translate("Form", "CHỤP ẢNH"))
        self.pushButton_2.setText(_translate("Form", "CẬP NHẬT CSDL"))
        self.label_2.setText(_translate("Form", "VUI LÒNG DÍ KHUÔN MẶT CỦA BẠN VÀO ĐÂY"))

    # Start image capture & display
    def start(self):
        self.timer = QTimer(self)  # Timer to trigger display
        self.timer.timeout.connect(lambda:
                                   self.show_image(image_queue, self.graphicsView, DISP_SCALE))
        self.timer.start(DISP_MSEC)
        self.capture_thread = threading.Thread(target=grab_images,
                                               args=(camera_num, image_queue))
        self.capture_thread.start()  # Thread to grab images

    # Fetch camera image from queue, and display it
    def show_image(self, imageq, display, scale):
        if not imageq.empty():
            image = imageq.get()
            if image is not None and len(image) > 0:
                img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                self.display_image(img, display, scale)

    # Display an image, reduce size if required
    def display_image(self, img, display, scale=1):
        print("aaa")
        disp_size = 561, 331
        disp_bpl = disp_size[0] * 3
        if scale > 1:
            img = cv2.resize(img, disp_size,
                             interpolation=cv2.INTER_CUBIC)
        qimg = QImage(img.data, disp_size[0], disp_size[1],
                      disp_bpl, IMG_FORMAT)
        display.setImage(qimg)

    # Handle sys.stdout.write: update text display
    def write(self, text):
        self.text_update.emit(str(text))

    def flush(self):
        pass

    # Append to text display
    def append_text(self, text):
        cur = self.textBrowser.textCursor()  # Move cursor to end of text
        cur.movePosition(QTextCursor.End)
        s = str(text)
        while s:
            head, sep, s = s.partition("\n")  # Split line at LF
            cur.insertText(head)  # Insert text at cursor
            if sep:  # New line if LF
                cur.insertBlock()
        self.textBrowser.setTextCursor(cur)  # Update visible cursor

    # Window is closing: stop video capture
    def closeEvent(self, event):
        global capturing
        capturing = False
        self.capture_thread.join()


class Face_recognize:
    face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    window_name = "window"
    tmp_path = "./images"
    font = cv2.FONT_HERSHEY_SIMPLEX

    verification_threshhold = 0.68
    image_size = 160
    v = ftk.Verification()
    # Pre-load model for Verification
    v.load_model("./models/20180204-160909/")
    v.initial_input_output_tensors()

    d = dtk.Detection()

    def img_to_encoding(self, img):
        return self.v.img_to_encoding(img, self.image_size)

    def img_file_to_encoding(self, img):
        image = imread(img)
        aligned = self.d.align(image, False)[0]
        return self.v.img_to_encoding(aligned, self.image_size)

    def load_database(self):
        database = {}
        database["alireza"] = self.img_file_to_encoding("./images/alireza.jpg")
        database["ali"] = self.img_file_to_encoding("./images/ali.jpg")
        database["quydz"] = self.img_file_to_encoding("./images/quydz.jpg")
        database["mohsen"] = self.img_file_to_encoding("./images/mohsen.jpg")
        database["muhammad"] = self.img_file_to_encoding("./images/muhammad.jpg")
        return database


    def distance(self, emb1, emb2):
        diff = np.subtract(emb1, emb2)
        return np.sum(np.square(diff))

    def verify(self, image_path, identity, database):

        # Step 1: Compute the encoding for the image. Use img_to_encoding()
        encoding = self.img_to_encoding(image_path)

        # Step 2: Compute distance with identity's image
        dist = self.distance(encoding, database[identity])

        # Step 3: Open the door if dist < verification_threshhold, else don't open
        if dist < self.verification_threshhold:
            print("It's " + str(identity) + ", welcome!")
        else:
            print("It's not " + str(identity) + ", please go away")

        return dist

    def who_is_it(self, image_path, database):

        ## Step 1: Compute the target "encoding" for the image. Use img_to_encoding()
        encoding = self.img_to_encoding(image_path)

        ## Step 2: Find the closest encoding ##

        # Initialize "min_dist" to a large value, say 100
        min_dist = 1000
        identity = "Not found"
        # Loop over the database dictionary's names and encodings.
        for (name, db_enc) in database.items():
            # Compute L2 distance between the target "encoding" and the current "emb" from the database. (≈ 1 line)
            dist = self.distance(encoding, db_enc)

            # If this distance is less than the min_dist, then set min_dist to dist, and identity to name. (≈ 3 lines)
            if min_dist > dist:
                min_dist = dist
                identity = name

        if min_dist > self.verification_threshhold:
            print("Not in the database.")
        else:
            print("it's " + str(identity) + ", the distance is " + str(min_dist))

        return min_dist, identity


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        try:
            camera_num = int(sys.argv[1])
        except:
            camera_num = 0
    if camera_num < 1:
        print("Invalid camera number '%s'" % sys.argv[1])
    else:
        app = QtWidgets.QApplication(sys.argv)
        Form = QtWidgets.QWidget()
        ui = Ui_Form()
        ui.setupUi(Form)
        ui.start()
        Form.showFullScreen()
        Form.show()

        sys.exit(app.exec_())

    #     app = QApplication(sys.argv)
    #     win = MyWindow()
    #     win.show()
    #     win.setWindowTitle(VERSION)
    #     win.start()
    #     sys.exit(app.exec_())