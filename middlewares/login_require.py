from flask import request
from werkzeug.wrappers import Request
from helpers.login import is_login
from werkzeug.exceptions import Unauthorized
import io

ExceptLogin = [
    '/api/register',
    '/api/login',
    '/api/facial_recognition',
    '/home',
    '/socket.io/',
    '/api/faces_store',
    '/api/face-dectector',
    '/api/get-user-images',
    '/api/captured-face'

]

class LoginRequire(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        req = Request(environ, shallow=True)
        token = req.args.get('access_token')
        print(environ['PATH_INFO'])
        if environ['PATH_INFO'] == '/socket.io/':
            return self.app(environ, start_response)

        if environ['PATH_INFO'].strip() in ExceptLogin:
            print('PATH_INFO')
            print(environ['PATH_INFO'])
            return self.app(environ, start_response)

        user = is_login(token)
        if user:
            return self.app(environ, start_response)
        else:
            return Unauthorized()(environ, start_response)