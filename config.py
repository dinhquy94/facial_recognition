import os
MONGO = {
    'HOST': 'mongodb://localhost:27017/',
    'DATABASE': 'codecamp_api'
}

REDIS = {
    'REDIS_HOST': 'localhost',
    'REDIS_PORT': 6379,
    'REDIS_DB': 0
}

APP_SECRET = 'quydzabcdefgh'
IMAGE_QUEUE = 'face_detector'
FACIAL_DETECTOR_QUEUE = 'facial_detector'
CLIENT_SLEEP = 0.001
BATCH_SIZE = 32
SERVER_SLEEP = 0.25


IMAGE_WIDTH = 224
IMAGE_HEIGHT = 224
IMAGE_CHANS = 3
IMAGE_DTYPE = "float32"


dir_path = os.path.dirname(os.path.realpath(__file__))
FACES_FOLDER = dir_path + '/images/faces/'