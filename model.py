import FaceToolKit as ftk
import DetectionToolKit as dtk
import matplotlib.pyplot as plt

verification_threshhold = 0.5
image_size = 160
v = ftk.Verification()
# Pre-load model for Verification
v.load_model("./models/20180204-160909/")
v.initial_input_output_tensors()


d = dtk.Detection()


def img_file_to_encoding(img):
    image = plt.imread(img)
    encoded = []
    encoded.append(v.img_to_encoding(image, image_size))
    return encoded

