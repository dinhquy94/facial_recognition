from mongoengine import *


class GroupFaces(Document):
    store_name = StringField(required=True, max_length=200)
    group_id = IntField(required=False, max_length=50)
    store_id = StringField(required=True, max_length=200, unique=True)
    description = StringField(required=False, max_length=400)
    user_id = StringField(required=True, max_length=50)
    api_key = StringField(required=False, max_length=50)
    count = IntField(required=False, max_length=50, default=0)
    quota = IntField(required=False, default=300000)
    meta = {'allow_inheritance': True}

