from mongoengine import *


class Users(Document):
    username = StringField(required=True, max_length=200, unique=True)
    password_hashed = StringField(required=True, max_length=200)
    email = EmailField(required=False, max_length=400)
    max_store = IntField(required=False, default=0)
    max_data_input = IntField(required=False, default=0)
    max_request_per_month = IntField(required=False, default=0)
    meta = {'allow_inheritance': True}
