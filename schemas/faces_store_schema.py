from jsonschema import validate
from datetime import datetime
import time
from mongoengine import *


schema = {
    'store_id': 'string',
    'face_id': 'string',
    'name': 'string',
    'description': 'string',
    'file_type': 'string'
}

def validate_input(input):
    try:
        validate(instance=input, schema=schema)
        return True, None
    except Exception as e:
        return False, str(e)


class FacesStores(Document):
    store_id = StringField(required=True, max_length=200)
    face_id = StringField(required=False, max_length=200)
    face_encoding = ListField(required=False)
    name = StringField(required=True, max_length=500)
    description = StringField(required=False, max_length=500)
    file_type = StringField(required=True, max_length=20)
    username_mapping = StringField(required=False, max_length=20)
    group_id = IntField(required=False)
    customer_type = StringField(required=False, max_length=400, default="New")
    date_of_birth = DateField(required=False)
    address = StringField(required=False, max_length=400)
    visit_count = IntField(required=False, default=0)
    firt_coming_time = IntField(required=False, default=time.time())
    last_coming_time = IntField(required=False, default=time.time())
    joining_time = IntField(required=False, default=time.time())
    avg_budget = IntField(required=False, default=0)
    total_spending = IntField(required=False, default=0)
    is_checkin = BooleanField(required=False, default=False)


    meta = {'allow_inheritance': True}

