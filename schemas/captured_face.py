from mongoengine import *


class CapturedFace(Document):
    face_id = StringField(required=True, max_length=200)
    store_id = StringField(required=False, max_length=200)
    description = StringField(required=False, max_length=400)
    meta = {'allow_inheritance': True}