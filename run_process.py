import numpy as np
from PIL import Image
import io
import config
import json
import uuid
from mongo import redis_db
from helpers import img_encoder
from schemas.captured_face import CapturedFace
import cv2

verification_threshhold = 0.5
image_size = 160

face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def dectector_process():
    # load the pre-trained Keras model (here we are using a model
    # pre-trained on ImageNet and provided by Keras, but you can
    # substitute in your own networks just as easily)
    print("* Loading model...")

    print("* Model loaded")

    # continually pool for new images to classify
    while True:
        # attempt to grab a batch of images from the database, then
        # initialize the image IDs and batch of images themselves
        queue = redis_db.lrange(config.IMAGE_QUEUE, 0,
                                config.BATCH_SIZE - 1)
        # imageIDs = []
        # batch = None

        # loop over the queue
        for q in queue:
            # deserialize the object and obtain the input image
            q = json.loads(q)
            image = img_encoder.base64_decode_image(q["image"])
            image = Image.open(io.BytesIO(image))
            image = np.array(image)
            faces_detected = []
            faces = []
            i = 0
            # faces_detected = d.align(image, detect_multiple_faces=True)
            faces_cv2 = face_detector.detectMultiScale(image, 1.3, 5)
            for (x, y, w, h) in faces_cv2:
                x1 = x
                y1 = y
                x2 = x + w
                y2 = y + h
                faces_detected.append(image[y1:y2, x1:x2])

            for face in faces_detected:
                i += 1
                k = str(uuid.uuid4()) + str(i)
                im = Image.fromarray(face)
                img_path = config.FACES_FOLDER + k + ".jpg"
                im.save(img_path)
                faces.append({
                    'face_id': k,
                    'url': 'images/faces/'+ k + '.jpg'
                })
                if q['store_id']:
                    captured_face = CapturedFace(
                        face_id=k,
                        store_id=q['store_id'],
                        description="Ảnh chụp từ thiết bị"
                    )
                    captured_face.save()

            redis_db.set(q["id"], json.dumps(faces))
            redis_db.ltrim(config.IMAGE_QUEUE, 1, -1)


if __name__ == "__main__":
    dectector_process()
