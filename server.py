
from flask import Flask, jsonify, url_for, redirect, request, send_from_directory
from flask_pymongo import PyMongo
from flask_restful import Api, Resource
from routes import route_login
from flask_cors import CORS

from routes.route_store import RouteStore
from middlewares.login_require import LoginRequire
from routes.route_assets_upload import RouteAssetsUpload
from routes.route_face_detector import RouteFaceDetector
from routes.route_store_update import RouteStoreUpdate
from routes.route_facial_recognition import RouteFacialRecognition
from routes.route_register import RouteRegister
from routes.route_customer import RouteUsername
from routes.route_captured_face import RouteCapturedFace

app = Flask(__name__)
cors = CORS(app, resources={"/api/*": {"origins": "*"}})

app.config["MONGO_DBNAME"] = "tweets_db"
mongo = PyMongo(app, 'mongodb://localhost:27017/')
APP_URL = "http://localhost:5000"




api = Api(app)
api.add_resource(route_login.Login, "/api/login", endpoint="login")
api.add_resource(RouteRegister, "/api/register", endpoint="register")
api.add_resource(RouteFaceDetector, "/api/face-dectector", endpoint="face_dectector")

app.wsgi_app = LoginRequire(app.wsgi_app)
api.add_resource(RouteStore, "/api/store", endpoint="store")
api.add_resource(RouteAssetsUpload, "/api/upload-assets", endpoint="asset")
api.add_resource(RouteStoreUpdate, "/api/faces_store", endpoint="faces_store")
api.add_resource(RouteFacialRecognition, "/api/facial_recognition", endpoint="facial_recognition")
api.add_resource(RouteUsername, "/api/get-user-images", endpoint="get_user_images")
api.add_resource(RouteCapturedFace, "/api/captured-face", endpoint="captured_face")

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=5050)

