import config

from pymongo import MongoClient
import redis

client = MongoClient(config.MONGO['HOST'])
db = client[config.MONGO['DATABASE']]

redis_db = redis.StrictRedis(host=config.REDIS['REDIS_HOST'],
                       port=config.REDIS['REDIS_PORT'], db=config.REDIS['REDIS_DB'])


from mongoengine import connect

connect(config.MONGO['DATABASE'])
